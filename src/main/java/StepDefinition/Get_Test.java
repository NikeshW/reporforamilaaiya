package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import Common.CommonMethods;
import ExtendReport.ExtentReport;
import ReusableRequest.GetRequest;
import Utility.JSONReader;

public class Get_Test {

	JSONReader _JSONRead = new JSONReader();
	CommonMethods objCommonMethods = new CommonMethods();
	ExtentReport extentReport = new ExtentReport();

	JsonPath JsonResponse;
	GetRequest _getReq;
	String Req_URL;

	@Given("^I have a URL to get category details$")
	public void getrequestURL() throws Throwable {

		// Setup the test case
		extentReport.setUpReport();

		// Providing the request url through a json file
		Req_URL = _JSONRead.ReadJSONFile("Request_Get", System.getProperty("user.dir") + "\\Data\\wsData.json");
		System.out.println("I have a correct GET request URL");
	}

	@When("^I successfully send the request$")
	public void getRequestAndResponse() throws Throwable {

		// Send the request and get the response as a Json
		_getReq = new GetRequest();
		JsonResponse = _getReq.getRequest_ResponseToJson(Req_URL);
		System.out.println("I have successfully submitted the GET request");
	}

	@Then("^Verify the Category Name is \"([^\"]*)\" in the received response$")
	public void verifyCategoryName(String CategoryName) throws Throwable {

		// Start Acceptance Criteria 1
		extentReport.startTestCase("Verify the category name");

		// validating whether the Category Name is "Carbon credits"
		Assert.assertEquals(objCommonMethods.getValuefromJsonResponse(JsonResponse, "Name"), CategoryName,
				"Correct category name is received in the Response");

		if (objCommonMethods.getValuefromJsonResponse(JsonResponse, "Name").equals(CategoryName)) {
			extentReport.logEventsPass("Category Name is correct: " + CategoryName);
			System.out.println(
					"The Name value returned is: " + objCommonMethods.getValuefromJsonResponse(JsonResponse, "Name"));
		} else {

			extentReport.logEventsFail("Category Name is not equals to " + CategoryName);
			System.out.println("Incorrect category name is displayed");
		}

	}

	@Then("^Verify the CanRelist of the category is \"([^\"]*)\" in the received response$")
	public void verifyCanRelistValue(Boolean existOrNot) throws Throwable {

		// Start Acceptance Criteria 2
		extentReport.startTestCase("Verify the CanRelist value");

		// Validate that the CanRelist is displayed as true in the response
		Assert.assertEquals(objCommonMethods.getBooleanValuefromResponse(JsonResponse, "CanRelist"), existOrNot,
				"Correct CanRelist option is received in the Response");

		if (objCommonMethods.getBooleanValuefromResponse(JsonResponse, "CanRelist").equals(existOrNot)) {

			extentReport.logEventsPass("CanRelist vslue is correct: " + existOrNot);
			System.out.println("The CanRelist vslue returned is: "
					+ objCommonMethods.getBooleanValuefromResponse(JsonResponse, "CanRelist"));
		} else {

			extentReport.logEventsFail("CanRelist value is not equals to " + existOrNot);
			System.out.println("Incorrect CanRelist value is displayed");
		}
	}

	@Then("^The element \"([^\"]*)\" has a Name \"([^\"]*)\"  with \"([^\"]*)\" contains \"([^\"]*)\" in the received response$")
	public void verifyDescriptionText(String Element, String Name, String ElementProperty, String ElementPropertyValue)
			throws Throwable {

		// Start Acceptance Criteria 3
		extentReport.startTestCase("Verify the " + ElementPropertyValue + "contains in " + ElementProperty);

		String NameKey = "Name";
		Assert.assertEquals(
				objCommonMethods.getResponseAttribute(JsonResponse, NameKey, Element, Name, ElementProperty,
						ElementPropertyValue),
				true, ElementPropertyValue + " contains in " + ElementProperty + " when the Name is " + Name
						+ " in the " + Element);

		if (objCommonMethods.getResponseAttribute(JsonResponse, NameKey, Element, Name, ElementProperty,
				ElementPropertyValue)) {

			extentReport.logEventsPass(
					ElementProperty + " contains " + ElementPropertyValue + " when promotion name is Gallery");
			System.out.println(ElementPropertyValue + " contains in " + ElementProperty + " when the Name is " + Name
					+ " in the " + Element + " is: " + objCommonMethods.getResponseAttribute(JsonResponse, NameKey,
							Element, Name, ElementProperty, ElementPropertyValue));
		} else {

			extentReport.logEventsFail(
					ElementProperty + " doesn't contain " + ElementPropertyValue + "when promotion name is Gallery");
			System.out.println(
					ElementProperty + " doesn't contain " + ElementPropertyValue + "when promotion name is Gallery");
		}

		extentReport.createFinalReport();
	}

}
