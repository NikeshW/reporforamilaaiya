package ExtendReport;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReport {

	ExtentReports extent;
    ExtentTest logger;
    
   //Setup the extend reporting
   public void setUpReport ()
   {
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/ExecutionReport.html", true);
   }
   //Log event in extent report when the test case is pass
   public void logEventsPass (String value)
   {
	   logger.log(LogStatus.PASS, value);
   }
   //Log event in exetent report when the test case is fail
   public void logEventsFail (String value)
   {
	   logger.log(LogStatus.FAIL, value);
   }
   
   //Starting the test case
   public void startTestCase (String testcaseName)
   {
	   logger = extent.startTest(testcaseName);
   }
   //Creating the final report in the given setup location
   public void createFinalReport()
   {
	   extent.flush();
   }
}
